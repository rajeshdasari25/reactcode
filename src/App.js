import React, { Component } from 'react';
import Loader from "./Loader";
import Footer from "./Footer";
import axios from "axios";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      formData: []
    }
  }

  componentDidMount() {
    var self = this;
    self.setState({ loader: true })
    axios.get('https://reqres.in/api/users')
      .then((response) => {
        if (response && response.data) {
          self.setState({
            formData: response.data
          });
        }
      })
      .catch(error => error)
  }


  render() {

    const { formData } = this.state;
    let TotalUsers = formData && formData.total;
    let footerUrl = formData && formData.support && formData.support.url;
    let footerText = formData && formData.support && formData.support.text;

    return (
      <div className='container'>
      <div className='row'>
        {/* <div className='col-lg-12'>
      <div className='col-6'>
              <h6>Total : {TotalUsers}</h6>
            </div> */}
        <div className='col-12'>
          <div className='row mt-3'>
            <div className='col-6'>
              <h6>Total : {TotalUsers}</h6>
            </div>
          </div>
          <div className='row'>
            {formData && formData.data && formData.data.length > 0 ?
              formData.data.map(function (elm, i) {
                return (
                  <div className='col-md-4 col-md-4' key={i}>
                    <div className='card'>
                      <img src={elm.avatar} />
                      <h4><span className='left'>{elm.first_name}</span><br />
                        <span >{elm.last_name}</span></h4>
                      <h6>{elm.email}</h6>
                    </div>
                  </div>
                )
              })
              :
              <Loader />
            }
          </div>
        </div>
       <Footer footerText={footerText} footerUrl={footerUrl} />
      </div>
      </div>
    );
  }
};
export default App;
