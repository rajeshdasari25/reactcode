import React from 'react';


const Footer = (props) => {
    return (
        <footer>
          <a href={props.footerUrl}>{props.footerText}</a>
        </footer>
    );
};

export default (Footer);