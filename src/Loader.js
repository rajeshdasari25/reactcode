import React from 'react';


const Loader = () => {
    return (
        <div className="loader_cont">
            <div className="loader_wrapper">
                <div className="loader4"></div>
            </div>
        </div>
    );
};

export default (Loader);